package com.example.myclient;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class TestFragment extends Fragment {
    private Button startButton;
    private EditText IPText;
    private Context mContext;
    private boolean isConnecting = false;
    private Thread mThreadClient = null;
    private Socket mSocketClient = null;
    private PrintWriter mPrintWriterClient = null;
    private String res = "";
    private TextView recvText, recvText1, recvText2;
    private TempControlView tempControl;
    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler()
    {
        @SuppressLint("SetTextI18n")
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);
            if(msg.what==4)
            {
                char[] arrs;
                arrs=res.toCharArray();//接收来自服务器的字符串
                if (arrs.length>=9) {
                    recvText1.setText("温度: " + arrs[2] + arrs[3] + "℃" + ' ');
                    recvText2.setText("湿度: " + arrs[7] + arrs[8] + "%" + ' ');
                }else {
                    showDialog("收到格式错误的数据:" + res);
                }
            }else if (msg.what==2){
                showDialog("连接失败，服务器走丢了");
                startButton.setText("开始连接");

            }else if (msg.what==1){
                showDialog("连接成功！");
                recvText.setText("已连接空调\n");
                IPText.setEnabled(false);//锁定ip地址和端口号
                isConnecting = true;
                startButton.setText("停止连接");
            }else if (msg.what==3){
                recvText.setText("断开连接\n");
            }else if (msg.what==5){
                recvText.setText("IP和端口号不能为空\n");
            }
            else if (msg.what==6){
                recvText.setText("IP地址不合法\n");
            }
        }
    };
    private Switch switch_c;
    private boolean flag = false;

    //连接到智能衣柜
    private View.OnClickListener StartClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if(isConnecting)
            {
                isConnecting=false;
                if(mSocketClient!=null)
                {
                    try{
                        mSocketClient.close();
                        mSocketClient = null;
                        if (mPrintWriterClient!=null){
                            mPrintWriterClient.close();
                            mPrintWriterClient = null;
                        }
                        mThreadClient.interrupt();
                        startButton.setText("开始连接");
                        IPText.setEnabled(true);//可以输入ip和端口号
                        recvText.setText("断开连接\n");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }else
            {
                mThreadClient = new Thread(mRunnable);
                mThreadClient.start();
            }
        }
    };
    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            String msgText = IPText.getText().toString();
            if(msgText.length()<=0)
            {
                Message msg = new Message();
                msg.what = 5;
                mHandler.sendMessage(msg);
                return;
            }
            int start = msgText.indexOf(":");
            if((start==-1)||(start+1>=msgText.length()))
            {
                Message msg = new Message();
                msg.what = 6;
                mHandler.sendMessage(msg);
                return;
            }
            String sIP= msgText.substring(0,start);
            String sPort = msgText.substring(start+1);
            int port = Integer.parseInt(sPort);

            BufferedReader mBufferedReaderClient;
            try
            {
                //连接服务器
                mSocketClient = new Socket();
                SocketAddress socAddress = new InetSocketAddress(sIP, port);
                mSocketClient.connect(socAddress, 2000);//设置超时时间为2秒
                //取得输入、输出流
                mBufferedReaderClient = new BufferedReader(new InputStreamReader(mSocketClient.getInputStream()));
                mPrintWriterClient = new PrintWriter(mSocketClient.getOutputStream(), true);
                Message msg = new Message();
                msg.what = 1;
                mHandler.sendMessage(msg);

            }catch (Exception e) {
                Message msg = new Message();
                msg.what = 2;
                mHandler.sendMessage(msg);
                return;
            }
            char[] buffer = new char[256];
            int count;

            while(true)
            {
                try
                {
                    if((count = mBufferedReaderClient.read(buffer))>0)
                    {
                        res = getInfoBuff(buffer,count)+"\n";
                        Message msg = new Message();
                        msg.what = 4;
                        mHandler.sendMessage(msg);
                    }
                }catch (Exception e) {
                    // TODO: handle exception
                    Message msg = new Message();
                    msg.what = 3;
                    mHandler.sendMessage(msg);
                }
            }
        }
    };

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.fragment_test, null);
        mContext = getContext();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()
                .penaltyLog()
                .build()
        );
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());

        IPText= view.findViewById(R.id.IPText);
        IPText.setText("192.168.1.127:8080");
        startButton= view.findViewById(R.id.StartConnect);
        startButton.setOnClickListener(StartClickListener);

        recvText= view.findViewById(R.id.tv1);
        recvText1= view.findViewById(R.id.textView3);
        recvText2= view.findViewById(R.id.textView4);

        tempControl = view.findViewById(R.id.temp_control);
        // 设置三格代表温度1度
        tempControl.setAngleRate(1);
        tempControl.setTemp(16, 30, 16);
        //设置旋钮是否可旋转
        tempControl.setCanRotate(true);
        tempControl.setOnTempChangeListener(new TempControlView.OnTempChangeListener() {
            @Override
            public void change(int temp) {
                switch (temp) {
                    case 16:
                        send("*16", 1);
                        break;
                    case 17:
                        send("*17", 1);
                        break;
                    case 18:
                        send("*18", 1);
                        break;
                    case 19:
                        send("*19", 1);
                        break;
                    case 20:
                        send("*20", 1);
                    case 21:
                        send("*21", 1);
                        break;
                    case 22:
                        send("*22", 1);
                        break;
                    case 23:
                        send("*23", 1);
                        break;
                    case 24:
                        send("*24", 1);
                        break;
                    case 25:
                        send("*25", 1);
                        break;
                    case 26:
                        send("*26", 1);
                        break;
                    case 27:
                        send("*27", 1);
                        break;
                    case 28:
                        send("*28", 1);
                        break;
                    case 29:
                        send("*29", 1);
                        break;
                    case 30:
                        send("*30", 1);
                        break;
                }
            }
        });

        tempControl.setOnClickListener(new TempControlView.OnClickListener() {
            @Override
            public void onClick(int temp) {
                switch (temp) {
                    case 16:
                        send("*16", 1);
                        break;
                    case 17:
                        send("*17", 1);
                        break;
                    case 18:
                        send("*18", 1);
                        break;
                    case 19:
                        send("*19", 1);
                        break;
                    case 20:
                        send("*20", 1);
                    case 21:
                        send("*21", 1);
                        break;
                    case 22:
                        send("*22", 1);
                        break;
                    case 23:
                        send("*23", 1);
                        break;
                    case 24:
                        send("*24", 1);
                        break;
                    case 25:
                        send("*25", 1);
                        break;
                    case 26:
                        send("*26", 1);
                        break;
                    case 27:
                        send("*27", 1);
                        break;
                    case 28:
                        send("*28", 1);
                        break;
                    case 29:
                        send("*29", 1);
                        break;
                    case 30:
                        send("*30", 1);
                        break;
                }
            }
        });
        switch_c = view.findViewById(R.id.switch_c);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            switch_c.setShowText(true);
        }
        switch_c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    switch_c.setSwitchTextAppearance(mContext, R.style.s_true);
                    switch_c.setShowText(true);
                    if (send("*ON", -1)) {
                        flag = true;
                    } else {
                        switch_c.setChecked(false);
                    }
                } else {

                    switch_c.setSwitchTextAppearance(mContext, R.style.s_false);
                    switch_c.setShowText(true);
                    if (send("*OFF", -2)) {
                        flag = false;
                        tempControl.setTemp(16, 30, 16);
                    } else {
                        switch_c.setChecked(false);

                    }
                }
            }
        });

        return view;
    }
    private  void showDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }

    private String getInfoBuff(char[] buff, int count) {
        char[] temp = new char[count];
        System.arraycopy(buff, 0, temp, 0, count);
        return new String(temp);
    }

    private boolean send(String msg, int position) {
        if (isConnecting && mSocketClient != null) {
            if ((position == -1) || (position == -2)) {
                try {
                    mPrintWriterClient.print(msg);
                    mPrintWriterClient.flush();
                    return true;
                } catch (Exception e) {
                    // TODO: handle exception
                    Toast.makeText(mContext, "发送异常" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            } else {
                if (flag) {
                    try {
                        mPrintWriterClient.print(msg);
                        mPrintWriterClient.flush();
                        return true;
                    } catch (Exception e) {
                        // TODO: handle exception
                        Toast.makeText(mContext, "发送异常" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showDialog("您还没有开启空调哦，请先开启空调吧！");
                    tempControl.setTemp(16, 30, 16);


                }
            }
        } else {
            showDialog("您还没有连接空调呢！");
            tempControl.setTemp(16, 30, 16);
        }
        return false;
    }

}
