# MyClient
#wifi版本
#安卓通过tcp/ip协议与单片机（ESPWiFi模块自带tcp协议栈）进行通信，通过旋钮可以对智能家居的家电进行调档控制等等，并能实时显示单片机传过来的信息
#该APP适合没有带红外射频的手机，通过WiFi实现和带有红外模块的单片机通信，从而实现远程控制空调的功能。

#该代码是文章 https://wangjinchan.blog.csdn.net/article/details/110650804 的源码。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/113834_27488dde_5201073.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/113851_1860d1d6_5201073.png "屏幕截图.png")
